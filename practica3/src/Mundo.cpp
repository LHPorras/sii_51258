// Editado por Luis H Porras
//
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#include "Mundo.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	for (int j=0; j<listaEsferas.size(); j++)
		delete listaEsferas[j];
	listaEsferas.clear();
	sprintf(buffer, "Fin del juego. \n");
	write(fd, buffer, strlen(buffer)+1);
	close(fd);
	munmap(pdatos, sizeof(*pdatos));
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	//esfera.Dibuja();
	for (int j=0; j<listaEsferas.size(); j++)
		listaEsferas[j]->Dibuja();

	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	//Pasado cierto tiempo añadir esferas o aumentar velocidad
	if(tics == 1000)
	{
		tics = 0;
		int dim = listaEsferas.size();
		for(int i = 0; i<dim ;i++)
		{	
			//Crear numero de esferas ya existentes
			//Esfera* p = new Esfera();
			//if(p != NULL)
				listaEsferas.push_back(new Esfera());
		
			//Resetear radios
			listaEsferas[i]->setRadio(0.5f);
		}
	}

	//Al alcanzar un valor de tiempo, el radio de la pelota disminuye
	if (tics%200 == 0) 
		for(int j = 0; j<listaEsferas.size();j++)
			listaEsferas[j]->setRadio(listaEsferas[j]->getRadio()-0.08);
	//esfera.setRadio(esfera.getRadio()-0.05);

	tics++;

	//Mover esferas
	for(int i = 0; i<listaEsferas.size();i++)listaEsferas[i]->Mueve(0.025f);
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	for(int i=0;i<paredes.size();i++)
	{	//Rebote esferas
		for(int j = 0; j<listaEsferas.size();j++)paredes[i].Rebota(*(listaEsferas[j]));
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	//Rebote esferas
	for(int j = 0; j<listaEsferas.size();j++)jugador1.Rebota(*(listaEsferas[j]));
	for(int j = 0; j<listaEsferas.size();j++)jugador2.Rebota(*(listaEsferas[j]));
	
	for(int j = 0; j<listaEsferas.size();j++)
	{
		if(fondo_izq.Rebota(*(listaEsferas[j])))
		{	
			for(int k = 0; k<listaEsferas.size();k++)
			{
				listaEsferas[k]->centro.x=0;
				listaEsferas[k]->centro.y=rand()/(float)RAND_MAX;
				listaEsferas[k]->velocidad.x=2+2*rand()/(float)RAND_MAX;
				listaEsferas[k]->velocidad.y=2+2*rand()/(float)RAND_MAX;
				puntos2++;
				//Escribe
				sprintf(buffer,"Jugador 2 marca 1 punto, lleva un total de %d", puntos2);
				write(fd, buffer, strlen(buffer)+1);
				
				//Limpiar esferas 
				listaEsferas.clear();
				Esfera* po = new Esfera();
				listaEsferas.push_back(po);
				listaEsferas[0]->setRadio(0.5f);
				tics = 0;
				
			}
		}

		if(fondo_dcho.Rebota(*(listaEsferas[j])))
		{
			for(int k = 0; k<listaEsferas.size();k++)
			{
				listaEsferas[k]->centro.x=0;
				listaEsferas[k]->centro.y=rand()/(float)RAND_MAX;
				listaEsferas[k]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
				listaEsferas[k]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
				puntos1++;
				//Escribe
				sprintf(buffer,"Jugador 1 marca 1 punto, lleva un total de %d", puntos1);
				write(fd, buffer, strlen(buffer)+1);
				
				//Limpiar esferas 
				listaEsferas.clear();
				Esfera* po = new Esfera();
				listaEsferas.push_back(po);
				listaEsferas[0]->setRadio(0.5f);
				tics = 0;
				
			}
		}

		if(puntos1 >= 3 || puntos2 >= 3)
		{
			// sprintf(buffer, "Fin del juego. \n");
			// write(fd, buffer, strlen(buffer)+1);
			exit(0);		
		}

		//Actualizar datos del bot
		int index = 0;
		for(int i = 0; i<listaEsferas.size(); i++) 
			if(listaEsferas[i]->centro.x < listaEsferas[index]->centro.x)
				index = i;
		
	 	pdatos->esfera = *(listaEsferas[index]);
	 	pdatos->raqueta = jugador1;

	 	// printf("Posicion raqueta tenis: %f\n", (pdatos->raqueta.y1 + pdatos->raqueta.y2)/2);
	 	// printf("Posicion esfera tenis: %f\n", pdatos->esfera.centro.y);
	 	// printf("Accion tenis: %d\n", pdatos->accion);

	 	//Pulsar tecla
	 	switch(pdatos->accion)
	 	{
	 		case 1: 
				OnKeyboardDown('w',0,0); 
				break;
	 		case -1: 
				OnKeyboardDown('s',0,0); 
				break;
	 		case 0: 
				break;
	 	}

	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
	}
}

void CMundo::Init()
{
	tics = 0;

	//Esferas
	Esfera* po = new Esfera();
	listaEsferas.push_back(po);

	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Fifo
	if((fd = open("/tmp/fifo", O_WRONLY)) == -1)
	{
		printf("Error al abrir fifo. Tenis. \n");
		close(fd);
		exit(0);
	}

	//Datos bot
	if((fd_bot = open("/tmp/fichero_bot", O_RDWR, 0777)) == -1)
	{
		printf("Error al abrir fichero bot. Tenis. \n");
		close(fd_bot);
		exit(0);
	}

	write(fd_bot, pdatos, sizeof(*pdatos));
	
	if ((pdatos = (DatosMemCompartida *) mmap(NULL, sizeof(*pdatos), PROT_WRITE|PROT_READ, MAP_SHARED, fd_bot, 0)) == MAP_FAILED)
	{
		printf("Error en la proyeccion de memoria. Tenis \n");
		munmap(pdatos, sizeof(*pdatos));
		close(fd_bot);
		exit(0);
	}
	close(fd_bot);
}
