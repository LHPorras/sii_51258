#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#define MAX 100

int main(int arg, char* argv[])
{

	int fd, aux;
	char buffer[MAX];

 	if (mkfifo("/tmp/fifo", 0777) == -1)
	{
 		printf("Error al crear el fifo\n");
 		unlink("/tmp/fifo");
		exit(0);
	}

	if((fd = open("/tmp/fifo", O_RDONLY)) == -1)
	{
 		printf("Error al crear abrir el fifo \n");
 		close(fd);
		exit(0);
	}

	while(1)
	{
		if((aux = read(fd, buffer, sizeof(buffer))) == -1)
		{
			printf("Error en la lectura del fifo \n");
			break;
		}
		else
		{
			//printf("Lectura correcta \n");
			if(buffer[0] == 'F')
			{
				//Final del juego
				printf("%s \n", buffer);
				break;
			}
			else
			{
				//printf("Lectura correcta no fin \n");
				printf("%s \n", buffer);
			}
		}
	}

	close(fd);
	unlink("/tmp/fifo");
	return 0;
}
