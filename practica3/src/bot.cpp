#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include "../include/DatosMemCompartida.h"

int main(int arg, char* argv[])
{
	int fd_bot;
	float centroR;
	DatosMemCompartida * pdatos;

	fd_bot = open("/tmp/fichero_bot", O_RDWR);
	if(fd_bot == -1)
	{
		printf("Error al abrir fichero. Bot.\n");
		close(fd_bot);
		exit(0);
	}
	else
	{
		pdatos = (DatosMemCompartida *) mmap(NULL, sizeof(*pdatos), PROT_WRITE|PROT_READ, MAP_SHARED, fd_bot, 0);
		if (pdatos == MAP_FAILED)
		{
			printf("Error en la proyeccion de memoria. Bot \n");
			munmap(pdatos, sizeof(*pdatos));
			exit(0);
		}
		close(fd_bot);
	}

	while(1)
	{
		centroR = (pdatos->raqueta.y1 + pdatos->raqueta.y2)/2;

		//Inteligencia del bot
	 	if(centroR < pdatos->esfera.centro.y)
	 		pdatos->accion = 1;
	 	else if(centroR > pdatos->esfera.centro.y) 
	 		pdatos->accion = -1;
	 	else
	 		pdatos->accion = 0;

	 	// printf("Posicion raqueta bot: %f\n", centroR);
	 	// printf("Posicion esfera bot: %f\n", pdatos->esfera.centro.y);
	 	// printf("Accion bot: %d\n", pdatos->accion);

		usleep(25000);
	}

	printf("Fin bot \n");

	munmap(pdatos, sizeof(*pdatos));
	return 0;
}
