// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>
#include <vector>

#include "Esfera.h"
//#include "Raqueta.h"
#include "Plano.h"
#include "DatosMemCompartida.h"

//Comentar para usar en Mac
//#include "glut.h"
//////////////////////////////

//Comentar para usar en Ubuntu
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <Glut/glut.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <GLUT/glcmolorut.h>
#else
#include <GL/glut.h>
#endif
//////////////////////////////

using namespace std;

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	vector <Esfera*> listaEsferas;
	Esfera esfera;
	vector <Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

//	DatosMemCompartida datos;
	DatosMemCompartida* pdatos;

	int tics;

	int puntos1;
	int puntos2;
	int fd;
	int fd_bot;
	char buffer[100];
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
